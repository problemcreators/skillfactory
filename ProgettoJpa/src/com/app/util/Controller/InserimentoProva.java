package com.app.util.Controller;

import com.app.model.DAO.CategoriaDao;
import com.app.model.Entity.CategoriaAttivita;

public class InserimentoProva {
	CategoriaDao catdao=new CategoriaDao();

	public InserimentoProva() {
	}

	public void provaInserimento() {
		CategoriaAttivita cat= new CategoriaAttivita();
		cat.setNome("CatDiProva");
		catdao.inserimento(cat);

	}
}
