package com.app.util.EntityFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityFactory {

	public EntityFactory() {
	}

	public static final boolean DEBUG = true;

	private static final EntityFactory singleton = new EntityFactory();

	protected EntityManagerFactory emf;

	public static EntityFactory getInstance() {
		return singleton;
	}


	public EntityManagerFactory getEntityManagerFactory() {
		if (emf == null)
			createEntityManagerFactory();
		return emf;
	}

	public void closeEntityManagerFactory() {
		if (emf != null) {
			emf.close();
			emf = null;
			if (DEBUG)
				System.out.println("n*** Persistence finished at " + new java.util.Date());
		}
	}

	protected void createEntityManagerFactory() {
		this.emf = Persistence.createEntityManagerFactory("ProjectJpa");
		if (DEBUG)
			System.out.println("n*** Persistence started at " + new java.util.Date());
	}
	
	public EntityManager getEntityManager(){
		return emf.createEntityManager();
	}
	
}

