package com.app.model.DAO;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import com.app.model.Entity.EventoFormativo;
import com.app.util.EntityFactory.EntityFactory;

public class EventiDao {
	
	EntityManager em = EntityFactory.getInstance().getEntityManager();
	
	public void inserimento(EventoFormativo evento) {
		em.persist(evento);
		em.flush();
		em.clear();
	}

	public EventoFormativo returnEventoById(int id) {
		EventoFormativo evento=em.find(EventoFormativo.class, id);
		return evento;
	}
	
	public void modifica(EventoFormativo evento) {
		em.merge(evento);
		em.flush();
		em.clear();
	}
	
	public void cancella(EventoFormativo evento) {
		em.remove(em.merge(evento));
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<EventoFormativo> visualizza() {
		ArrayList<EventoFormativo> eventi= (ArrayList<EventoFormativo>)em.createQuery("From EventoFormativo").getResultList();
		return eventi;
	}
	
}
