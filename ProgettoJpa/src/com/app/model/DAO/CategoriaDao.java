package com.app.model.DAO;

import java.util.ArrayList;

import javax.persistence.EntityManager;

import com.app.model.Entity.CategoriaAttivita;
import com.app.util.EntityFactory.EntityFactory;

public class CategoriaDao {
	
EntityManager em = EntityFactory.getInstance().getEntityManager();
	
	public void inserimento(CategoriaAttivita categoria) {
		em.persist(categoria);
		em.flush();
		em.clear();
	}

	public CategoriaAttivita returnCategoriaById(int id) {
		CategoriaAttivita categoria=em.find(CategoriaAttivita.class, id);
		return categoria;
	}
	
	public void modifica(CategoriaAttivita categoria) {
		em.merge(categoria);
		em.flush();
		em.clear();
	}
	
	public void cancella(CategoriaAttivita categoria) {
		em.remove(em.merge(categoria));
		em.flush();
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<CategoriaAttivita> visualizza() {
		ArrayList<CategoriaAttivita> categorie= (ArrayList<CategoriaAttivita>)em.createQuery("From CategoriaAttivita").getResultList();
		return categorie;
	}

}
