package com.app.model.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@Table(name = "ATTIVITA_FORMATIVE")
public class AttivitaFormativa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ATT_FORM_SEQ", sequenceName = "att_form_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ATT_FORM_SEQ")
	private int id;
	
	@Column(name = "descrizione")
	private String descrizione;
	
	@Column(name = "prerequisito")
	private String prerequisiti;
	
	@Column(name = "target")
	private String target;

	@Column(name = "contenuto")
	private String contenuti;
	
	@ManyToOne()
	@JoinColumn(name = "id_categoria", referencedColumnName = "id")
	private CategoriaAttivita categoria;
	
	@ManyToOne()
	@JoinColumn(name = "id_settore", referencedColumnName = "id")
	private SettoreAttivita settore;
	
	@ManyToOne()
	@JoinColumn(name = "id_specializzazione", referencedColumnName = "id")
    private Specializzazione specializzazione;
	
	@OneToMany(mappedBy = "attivita", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EventoFormativo> eventiFormativi;

	public AttivitaFormativa() {
		super();
		this.id = 0;
		this.descrizione = "";
		this.prerequisiti = "";
		this.target = "";
		this.contenuti = "";
		this.categoria = new CategoriaAttivita();
		this.settore = new SettoreAttivita();
		this.specializzazione = new Specializzazione();
		this.eventiFormativi = new HashSet<EventoFormativo>();
	}
	
	public AttivitaFormativa(int id, String descrizione, String prerequisiti, String target, String contenuti,
			                 CategoriaAttivita categoria, SettoreAttivita settore, Specializzazione specializzazione,
			                 Set<EventoFormativo> eventiFormativi) {
		super();
		this.id = id;
		this.descrizione = descrizione;
		this.prerequisiti = prerequisiti;
		this.target = target;
		this.contenuti = contenuti;
		this.categoria = categoria;
		this.settore = settore;
		this.specializzazione = specializzazione;
		this.eventiFormativi = eventiFormativi;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDescrizione() {
		return descrizione;
	}
	
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	
	public String getPrerequisiti() {
		return prerequisiti;
	}
	
	public void setPrerequisiti(String prerequisiti) {
		this.prerequisiti = prerequisiti;
	}
	
	public String getTarget() {
		return target;
	}
	
	public void setTarget(String target) {
		this.target = target;
	}
	
	public String getContenuti() {
		return contenuti;
	}
	
	public void setContenuti(String contenuti) {
		this.contenuti = contenuti;
	}
	
	public CategoriaAttivita getCategoria() {
		return categoria;
	}
	
	public void setCategoria(CategoriaAttivita categoria) {
		this.categoria = categoria;
	}
	
	public SettoreAttivita getSettore() {
		return settore;
	}
	
	public void setSettore(SettoreAttivita settore) {
		this.settore = settore;
	}
	
	public Specializzazione getSpecializzazione() {
		return specializzazione;
	}
	
	public void setSpecializzazione(Specializzazione specializzazione) {
		this.specializzazione = specializzazione;
	}

	public Set<EventoFormativo> getEventi() {
		return eventiFormativi;
	}

	public void setEventi(Set<EventoFormativo> eventi) {
		this.eventiFormativi = eventi;
	}
	
	public void addEvento(EventoFormativo evento) {
		eventiFormativi.add(evento);
		if (evento != null) {
			evento.setAttivita(this);
		}
	}
	
	public void removeEvento(EventoFormativo evento) {
		eventiFormativi.remove(evento);
		if (evento != null) {
			evento.setAttivita(null);
		}
	}

	// per generare hashcode e equals ho deselezionato l'id e le collezioni
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categoria == null) ? 0 : categoria.hashCode());
		result = prime * result + ((contenuti == null) ? 0 : contenuti.hashCode());
		result = prime * result + ((descrizione == null) ? 0 : descrizione.hashCode());
		result = prime * result + ((prerequisiti == null) ? 0 : prerequisiti.hashCode());
		result = prime * result + ((settore == null) ? 0 : settore.hashCode());
		result = prime * result + ((specializzazione == null) ? 0 : specializzazione.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AttivitaFormativa)) {
			return false;
		}
		AttivitaFormativa other = (AttivitaFormativa) obj;
		if (categoria == null) {
			if (other.categoria != null) {
				return false;
			}
		} else if (!categoria.equals(other.categoria)) {
			return false;
		}
		if (contenuti == null) {
			if (other.contenuti != null) {
				return false;
			}
		} else if (!contenuti.equals(other.contenuti)) {
			return false;
		}
		if (descrizione == null) {
			if (other.descrizione != null) {
				return false;
			}
		} else if (!descrizione.equals(other.descrizione)) {
			return false;
		}
		if (prerequisiti == null) {
			if (other.prerequisiti != null) {
				return false;
			}
		} else if (!prerequisiti.equals(other.prerequisiti)) {
			return false;
		}
		if (settore == null) {
			if (other.settore != null) {
				return false;
			}
		} else if (!settore.equals(other.settore)) {
			return false;
		}
		if (specializzazione == null) {
			if (other.specializzazione != null) {
				return false;
			}
		} else if (!specializzazione.equals(other.specializzazione)) {
			return false;
		}
		if (target == null) {
			if (other.target != null) {
				return false;
			}
		} else if (!target.equals(other.target)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "AttivitaFormativa [id=" + id + ", descrizione=" + descrizione + ", prerequisiti=" + prerequisiti
				+ ", target=" + target + ", contenuti=" + contenuti + ", categoria=" + categoria + ", settore="
				+ settore + ", specializzazione=" + specializzazione + "]";
	}
	
	
}
