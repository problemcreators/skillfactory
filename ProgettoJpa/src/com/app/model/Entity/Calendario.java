package com.app.model.Entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CALENDARI")
public class Calendario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CALENDARIO_SEQ", sequenceName = "calendario_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CALENDARIO_SEQ")
	private int id;
	
	@Column(name = "data_inizio")
	private LocalDate dataInizio;

	@Column(name = "data_fine")
	private LocalDate dataFine;
	
	@Column(name = "localita")
	private String localita;
	
	@OneToMany(mappedBy = "calendario", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EventoFormativo> eventiFormativi;
	
	public Calendario() {
		super();
		this.id = 0;
		this.dataInizio = LocalDate.of(0, 0, 0);
		this.dataFine = LocalDate.of(0, 0, 0);
		this.localita = "";
		this.eventiFormativi = new HashSet<EventoFormativo>();
	}

	public Calendario(int id, LocalDate dataInizio, LocalDate dataFine, String localita, Set<EventoFormativo> eventi) {
		super();
		this.id = id;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
		this.localita = localita;
		this.eventiFormativi = eventi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public LocalDate getDataInizio() {
		return dataInizio;
	}

	public void setDataInizio(LocalDate dataInizio) {
		this.dataInizio = dataInizio;
	}

	public LocalDate getDataFine() {
		return dataFine;
	}

	public void setDataFine(LocalDate dataFine) {
		this.dataFine = dataFine;
	}

	public String getLocalita() {
		return localita;
	}

	public void setLocalita(String localita) {
		this.localita = localita;
	}

	public Set<EventoFormativo> getEventi() {
		return eventiFormativi;
	}

	public void setEventi(Set<EventoFormativo> eventi) {
		this.eventiFormativi = eventi;
	}

	public Set<EventoFormativo> getEventiFormativi() {
		return eventiFormativi;
	}

	public void setEventiFormativi(Set<EventoFormativo> eventiFormativi) {
		this.eventiFormativi = eventiFormativi;
	}
	
	public void addEvento(EventoFormativo evento) {
		eventiFormativi.add(evento);
		if (evento != null) {
			evento.setCalendario(this);
		}
	}
	
	public void removeEvento(EventoFormativo evento) {
		eventiFormativi.remove(evento);
		if (evento != null) {
			evento.setCalendario(null);
		}
	}

	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataFine == null) ? 0 : dataFine.hashCode());
		result = prime * result + ((dataInizio == null) ? 0 : dataInizio.hashCode());
		result = prime * result + ((localita == null) ? 0 : localita.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Calendario)) {
			return false;
		}
		Calendario other = (Calendario) obj;
		if (dataFine == null) {
			if (other.dataFine != null) {
				return false;
			}
		} else if (!dataFine.equals(other.dataFine)) {
			return false;
		}
		if (dataInizio == null) {
			if (other.dataInizio != null) {
				return false;
			}
		} else if (!dataInizio.equals(other.dataInizio)) {
			return false;
		}
		if (localita == null) {
			if (other.localita != null) {
				return false;
			}
		} else if (!localita.equals(other.localita)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "Calendario [id=" + id + ", dataInizio=" + dataInizio + ", dataFine=" + dataFine + ", localita="
				+ localita + "]";
	}
	
	

}
