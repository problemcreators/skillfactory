package com.app.model.Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EVENTI_FORMATIVI")
public class EventoFormativo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "EVENTO_SEQ", sequenceName = "evento_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evento_SEQ")
	private int id ;
	
	@Column(name = "prezzo")
	private double prezzo;
	
	@ManyToOne()
	@JoinColumn(name = "id_attivita_formativa", referencedColumnName = "id")
	private AttivitaFormativa attivita;
	
	@ManyToOne()
	@JoinColumn(name = "id_modalita_erogazione", referencedColumnName = "id")
	private ModalitaErogazione modalita;
	
	@ManyToOne()
	@JoinColumn(name = "id_calendario", referencedColumnName = "id")
	private Calendario calendario;
	
	public EventoFormativo() {
		super();
		this.id = 0;
		this.prezzo = 0.0;
		this.attivita = new AttivitaFormativa();
		this.modalita = new ModalitaErogazione();
		this.calendario = new Calendario();
	}
	
	public EventoFormativo(int id, AttivitaFormativa attivitaFormativa, ModalitaErogazione modalitaErogazione, double prezzo,
			               Calendario calendario) {
		super();
		this.id = id;
		this.prezzo = prezzo;
		this.attivita = attivitaFormativa;
		this.modalita = modalitaErogazione;
		this.calendario = calendario;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public AttivitaFormativa getAttivita() {
		return attivita;
	}
	
	public void setAttivita(AttivitaFormativa attivitaFormativa) {
		this.attivita = attivitaFormativa;
	}
	
	public ModalitaErogazione getModalita() {
		return modalita;
	}
	
	public void setModalita(ModalitaErogazione modalitaErogazione) {
		this.modalita = modalitaErogazione;
	}
	
	public double getPrezzo() {
		return prezzo;
	}
	
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
	public Calendario getCalendario() {
		return calendario;
	}
	
	public void setCalendario(Calendario calendario) {
		this.calendario = calendario;
	}


	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((attivita == null) ? 0 : attivita.hashCode());
		result = prime * result + ((calendario == null) ? 0 : calendario.hashCode());
		result = prime * result + ((modalita == null) ? 0 : modalita.hashCode());
		long temp;
		temp = Double.doubleToLongBits(prezzo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof EventoFormativo)) {
			return false;
		}
		EventoFormativo other = (EventoFormativo) obj;
		if (attivita == null) {
			if (other.attivita != null) {
				return false;
			}
		} else if (!attivita.equals(other.attivita)) {
			return false;
		}
		if (calendario == null) {
			if (other.calendario != null) {
				return false;
			}
		} else if (!calendario.equals(other.calendario)) {
			return false;
		}
		if (modalita == null) {
			if (other.modalita != null) {
				return false;
			}
		} else if (!modalita.equals(other.modalita)) {
			return false;
		}
		if (Double.doubleToLongBits(prezzo) != Double.doubleToLongBits(other.prezzo)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "EventoFormativo [id=" + id + ", prezzo=" + prezzo + ", attivita=" + attivita + ", modalita=" + modalita
				+ ", calendario=" + calendario + "]";
	}
	
}
