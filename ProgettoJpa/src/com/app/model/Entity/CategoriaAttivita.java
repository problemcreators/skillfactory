package com.app.model.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORIE_ATTIVITA")
public class CategoriaAttivita implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "CAT_ATT_SEQ", sequenceName = "cat_att_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CAT_ATT_SEQ")
	private int id;
	
	@Column(name = "nome_categoria")
	private String nomeCategoria;
	
	@OneToMany(mappedBy = "categoria", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<AttivitaFormativa> attivitaFormative;
	
	public CategoriaAttivita() {
		super();
		this.id = 0;
		this.nomeCategoria = "";
		this.attivitaFormative = new HashSet<AttivitaFormativa>();
	}

	public CategoriaAttivita(int id, String categoria, Set<AttivitaFormativa> attivitaFormative) {
		super();
		this.id = id;
		this.nomeCategoria = categoria;
		this.attivitaFormative = attivitaFormative;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nomeCategoria;
	}

	public void setNome(String categoria) {
		this.nomeCategoria = categoria;
	}

	public Set<AttivitaFormativa> getAttivitaFormative() {
		return attivitaFormative;
	}

	public void setAttivitaFormative(Set<AttivitaFormativa> attivitaFormative) {
		this.attivitaFormative = attivitaFormative;
	}
	
	public void addAttivita(AttivitaFormativa attivita) {
		attivitaFormative.add(attivita);
		if (attivita != null) {
			attivita.setCategoria(this);
		}
	}
	
	public void removeAttivita(AttivitaFormativa attivita) {
	    attivitaFormative.remove(attivita);
		if (attivita != null) {
			attivita.setCategoria(null);
		}
	}


	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeCategoria == null) ? 0 : nomeCategoria.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CategoriaAttivita)) {
			return false;
		}
		CategoriaAttivita other = (CategoriaAttivita) obj;
		if (nomeCategoria == null) {
			if (other.nomeCategoria != null) {
				return false;
			}
		} else if (!nomeCategoria.equals(other.nomeCategoria)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "CategoriaAttivita [id=" + id + ", nomeCategoria=" + nomeCategoria + "]";
	}
	
	
}
