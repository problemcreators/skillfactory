package com.app.model.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "MODALITA_EROGAZIONI")
public class ModalitaErogazione implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "MOD_EROGAZIONE_SEQ", sequenceName = "mod_erogazione_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MOD_EROGAZIONE_SEQ")
	private int id;
	
	@Column(name = "nome_modalita")
	private String nomeModalita;
	
	@OneToMany(mappedBy = "modalita", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<EventoFormativo> eventiFormativi;

	public ModalitaErogazione() {
		super();
		this.id = 0;
		this.nomeModalita = "";
		this.eventiFormativi = new HashSet<EventoFormativo>();
	}

	public ModalitaErogazione(int id, String nomeModalita, Set<EventoFormativo> eventi) {
		super();
		this.id = id;
		this.nomeModalita = nomeModalita;
		this.eventiFormativi = eventi;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNomeModalita() {
		return nomeModalita;
	}

	public void setNomeModalita(String nomeModalita) {
		this.nomeModalita = nomeModalita;
	}

	public Set<EventoFormativo> getEventi() {
		return eventiFormativi;
	}

	public void setEventi(Set<EventoFormativo> eventi) {
		this.eventiFormativi = eventi;
	}

	public void addEvento(EventoFormativo evento) {
		eventiFormativi.add(evento);
		if (evento != null) {
			evento.setModalita(this);
		}
	}
	
	public void removeEvento(EventoFormativo evento) {
		eventiFormativi.remove(evento);
		if (evento != null) {
			evento.setModalita(null);
		}
	}

	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeModalita == null) ? 0 : nomeModalita.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ModalitaErogazione)) {
			return false;
		}
		ModalitaErogazione other = (ModalitaErogazione) obj;
		if (nomeModalita == null) {
			if (other.nomeModalita != null) {
				return false;
			}
		} else if (!nomeModalita.equals(other.nomeModalita)) {
			return false;
		}
		return true;
	}


	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "ModalitaErogazione [id=" + id + ", nomeModalita=" + nomeModalita + "]";
	}
	
	
}
