package com.app.model.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SETTORI_ATTIVITA")
public class SettoreAttivita implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SETTORE_SEQ", sequenceName = "settore_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SETTORE_SEQ")
	private int id;
	
	@Column(name = "nome_settore")
	private String nomeSettore;
	
	@OneToMany(mappedBy = "settore", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Specializzazione> indirizziSettore;
	
	@OneToMany(mappedBy = "settore", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<AttivitaFormativa> attivitaFormative;
	
	public SettoreAttivita() {
		super();
		this.id = 0;
		this.nomeSettore = "";
		this.indirizziSettore = new HashSet<Specializzazione>();
		this.attivitaFormative = new HashSet<AttivitaFormativa>();
	}

	public SettoreAttivita(int id, String descrizione, Set<Specializzazione> indirizziSettore,
			Set<AttivitaFormativa> attivitaFormative) {
		super();
		this.id = id;
		this.nomeSettore = descrizione;
		this.indirizziSettore = indirizziSettore;
		this.attivitaFormative = attivitaFormative;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nomeSettore;
	}

	public void setNome(String descrizione) {
		this.nomeSettore = descrizione;
	}

	public Set<Specializzazione> getIndirizziSettore() {
		return indirizziSettore;
	}

	public void setIndirizziSettore(Set<Specializzazione> indirizziSettore) {
		this.indirizziSettore = indirizziSettore;
	}

	public Set<AttivitaFormativa> getAttivitaFormative() {
		return attivitaFormative;
	}

	public void setAttivitaFormative(Set<AttivitaFormativa> attivitaFormative) {
		this.attivitaFormative = attivitaFormative;
	}
	
	public void addIndirizzo(Specializzazione indirizzo) {
		indirizziSettore.add(indirizzo);
		if (indirizzo != null) {
			indirizzo.setSettore(this);
		}
	}
	
	public void removeIndirizzo(Specializzazione indirizzo) {
		indirizziSettore.remove(indirizzo);
		if (indirizzo != null) {
			indirizzo.setSettore(null);
		}
	}
	
	public void addAttivita(AttivitaFormativa attivita) {
		attivitaFormative.add(attivita);
		if (attivita != null) {
			attivita.setSettore(this);
		}
	}
	
	public void removeAttivita(AttivitaFormativa attivita) {
		attivitaFormative.remove(attivita);
		if (attivita != null) {
			attivita.setSettore(null);
		}
	}

	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeSettore == null) ? 0 : nomeSettore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SettoreAttivita)) {
			return false;
		}
		SettoreAttivita other = (SettoreAttivita) obj;
		if (nomeSettore == null) {
			if (other.nomeSettore != null) {
				return false;
			}
		} else if (!nomeSettore.equals(other.nomeSettore)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "SettoreAttivita [id=" + id + ", nomeSettore=" + nomeSettore + "]";
	}


}
