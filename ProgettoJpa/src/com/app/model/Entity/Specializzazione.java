package com.app.model.Entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "SPECIALIZZAZIONI")
public class Specializzazione implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "SPECIALIZZAZIONE_SEQ", sequenceName = "specializzazione_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SPECIALIZZAZIONE_SEQ")
	private int id;
	
	@Column(name = "nome_specializzazione")
	private String nomeSpecializzazione;
	
	@ManyToOne()
	@JoinColumn(name = "id_settore", referencedColumnName = "id")
	private SettoreAttivita settore;
	
	@OneToMany(mappedBy = "specializzazione", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<AttivitaFormativa> attivitaFormative;
	
	public Specializzazione() {
		super();
		this.id = 0;
		this.nomeSpecializzazione = "";
		this.settore = new SettoreAttivita();
		this.attivitaFormative = new HashSet<AttivitaFormativa>();	
	}

	public Specializzazione(int id, String specializzazione, SettoreAttivita settoreAttivita,
			Set<AttivitaFormativa> attivitaFormative) {
		super();
		this.id = id;
		this.nomeSpecializzazione = specializzazione;
		this.settore = settoreAttivita;
		this.attivitaFormative = attivitaFormative;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nomeSpecializzazione;
	}

	public void setNome(String nomeSpecializzazione) {
		this.nomeSpecializzazione = nomeSpecializzazione;
	}

	public SettoreAttivita getSettore() {
		return settore;
	}

	public void setSettore(SettoreAttivita settoreAttivita) {
		this.settore = settoreAttivita;
	}

	public Set<AttivitaFormativa> getAttivitaFormative() {
		return attivitaFormative;
	}

	public void setAttivitaFormative(Set<AttivitaFormativa> attivitaFormative) {
		this.attivitaFormative = attivitaFormative;
	}
	
	public void addAttivita(AttivitaFormativa attivita) {
		attivitaFormative.add(attivita);
		if (attivita != null) {
			attivita.setSpecializzazione(this);
		}
	}
	
	public void removeAttivita(AttivitaFormativa attivita) {
		attivitaFormative.remove(attivita);
		if (attivita != null) {
			attivita.setSpecializzazione(null);
		}
	}

	// per generare hashcode e equals ho deselezionato l'id e le collezioni

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nomeSpecializzazione == null) ? 0 : nomeSpecializzazione.hashCode());
		result = prime * result + ((settore == null) ? 0 : settore.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Specializzazione)) {
			return false;
		}
		Specializzazione other = (Specializzazione) obj;
		if (nomeSpecializzazione == null) {
			if (other.nomeSpecializzazione != null) {
				return false;
			}
		} else if (!nomeSpecializzazione.equals(other.nomeSpecializzazione)) {
			return false;
		}
		if (settore == null) {
			if (other.settore != null) {
				return false;
			}
		} else if (!settore.equals(other.settore)) {
			return false;
		}
		return true;
	}

	// non ho incluso le collezioni
	
	@Override
	public String toString() {
		return "Specializzazione [id=" + id + ", nomeSpecializzazione=" + nomeSpecializzazione + ", settore=" + settore
				+ "]";
	}

}
